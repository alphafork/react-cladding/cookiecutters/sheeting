# {{ cookiecutter.project_name }}

{{ cookiecutter.project_short_description }}


## License

{% if cookiecutter.license == "AGPL-3.0-or-later" %}[AGPL-3.0-or-later](LICENSE){% else %}{{ cookiecutter.license }}{% endif %}


## Contact

{{ cookiecutter.author_name }}

[{{ cookiecutter.author_email }}](mailto:{{ cookiecutter.author_email }})
