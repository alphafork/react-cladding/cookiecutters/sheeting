import React from 'react';

function {{ cookiecutter.project_slug | to_camel }}() {
  return (
    <div>
      <h1>{{ cookiecutter.project_name }}</h1>
    </div>
  );
}

export default {{ cookiecutter.project_slug | to_camel }};
