# Sheeting - React Component Library Template

[Cookiecutter](https://github.com/cookiecutter/cookiecutter) template for
bootstrapping a React component library.


## Quick Start

Install `cookiecutter` and `jinja2_strcase` (used for converting string case) if not alreay installed:

`pip install --user cookiecutter jinja2_strcase`

Generate React app using the Sheeting cookiecutter:

`cookiecutter https://gitlab.com/alphafork/react-cladding/cookiecutters/sheeting`


## Features
- A basic React app created using [Vite](https://github.com/vitejs/vite)
  with demo contents removed and necessary options added to convert it into a
  library.
- AGPL-3.0-or-later as the default license if not specified otherwise.
- README.md template with the following sections:
  - Project name as main heading and short description.
  - License section (with link to LICENSE file if AGPL-3.0-or-later is used).
  - Contact section with author's name and email.
- Post-generate [cookiecutter
  hook](https://cookiecutter.readthedocs.io/en/stable/advanced/hooks.html) to
  autoamtically initialize git, set up git hooks and install all dependencies.
- Linting and formatting using [ESLint and
  prettier](https://gitlab.com/alphafork/react-cladding/utils/eslint-config-sheeting),
  configured to run automatically on staged files using
  [husky](https://github.com/typicode/husky) and
  [lint-staged](https://github.com/okonet/lint-staged).
- [PrimeReact](https://primereact.org) UI component libraries pre-installed.
- [Sass](https://github.com/sass/sass) support.


## License

[GPL-3.0-or-later](LICENSE)


## Contact

[Alpha Fork Technologies](https://alphafork.com)

Email: [connect@alphafork.com](mailto:connect@alphafork.com)
